# SelectStageMMM

Website for MegamanMaker for Select Stage like original game

# Dependancies

|Name|Version|Describ|
|Angular 2+|8.x.x|Front-End|
|||
|||
|||

# Resources

All resources used for created this projet.

|Name|Describ|
||

# Features

*  Load from local or external JSON file with the configuration.
*  Configurator edito, as know as CFG-E.
*  Select a stage and open the game. (CFG-E)
*  Select background. (CFG-E)
*  Select Music from local or youtube (CFG-E)
*  Store progression in localStorage of browser.
*  Can store progression in cloud.
*  Form to validate sucess of level.

# Link

[MegamanMaker](https://megamanmaker.com/)
